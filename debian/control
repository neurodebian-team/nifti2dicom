Source: nifti2dicom
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Daniele E. Domenichelli <ddomenichelli@drdanz.it>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               cmake,
               qtbase5-dev,
               libinsighttoolkit4-dev,
               libfftw3-dev,
               libtclap-dev,
               libvtk9-qt-dev
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/med-team/nifti2dicom
Vcs-Git: https://salsa.debian.org/med-team/nifti2dicom.git
Homepage: https://github.com/biolab-unige/nifti2dicom
Rules-Requires-Root: no

Package: nifti2dicom-data
Architecture: all
Depends: ${misc:Depends}
Description: data files for nifti2dicom
 This package contains architecture-independent supporting data files
 required for use with nifti2dicom, such as such as documentation, icons,
 and translations.

Package: nifti2dicom
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         nifti2dicom-data (= ${source:Version})
Description: convert 3D medical images to DICOM 2D series
 Nifti2Dicom is a conversion tool that converts 3D NIfTI files (and other
 formats supported by ITK, including Analyze, MetaImage Nrrd and VTK)
 to DICOM.
 Unlike other conversion tools, it can import a DICOM file that is used
 to import the patient and study DICOM tags, and allows you to edit the
 accession number and other DICOM tags, in order to create a valid DICOM
 that can be imported in a PACS.
 .
 This package includes the command line tools.

Package: qnifti2dicom
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         nifti2dicom (= ${binary:Version}),
         nifti2dicom-data (= ${source:Version})
Description: convert 3D medical images to DICOM 2D series (gui)
 Nifti2Dicom is a conversion tool that converts 3D NIfTI files (and other
 formats supported by ITK, including Analyze, MetaImage Nrrd and VTK)
 to DICOM.
 Unlike other conversion tools, it can import a DICOM file that is used
 to import the patient and study DICOM tags, and allows you to edit the
 accession number and other DICOM tags, in order to create a valid DICOM
 that can be imported in a PACS.
 .
 This package contains the Qt5 GUI.
